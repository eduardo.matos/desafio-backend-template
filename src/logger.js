const log4js = require('log4js');

log4js.configure({
  appenders: {
    console: {
      type: 'stdout',
      layout: {
        type: 'pattern',
        pattern: '%m',
      },
    },
  },
  categories: { default: { appenders: ['console'], level: 'info' } },
});

module.exports = log4js.getLogger('desafio-code-review');
