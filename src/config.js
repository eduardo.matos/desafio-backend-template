module.exports = {
  port: process.env.PORT || 3000,
  dbUrl: process.env.DB_URL || 'sqlite::memory',
};
