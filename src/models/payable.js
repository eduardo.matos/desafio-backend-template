const Sequelize = require('sequelize');
const Transaction = require('./transaction');
const db = require('./db');

const Payable = db.define('payable', {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER,
  },
  status: {
    allowNull: false,
    type: Sequelize.STRING,
    defaultValue: 'processing',
  },
  transaction_id: {
    type: Sequelize.INTEGER,
    allowNull: false,
    references: {
      model: 'transactions',
      key: 'id',
    },
  },
  fee: {
    allowNull: false,
    type: Sequelize.INTEGER,
  },
  payment_date: {
    type: Sequelize.DATE,
    allowNull: false,
  },
}, {
  timestamps: false,
});

Payable.belongsTo(Transaction, { foreignKey: 'transaction_id', onDelete: 'CASCADE' });
module.exports = Payable;
