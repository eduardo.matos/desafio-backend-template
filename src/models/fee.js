const Sequelize = require('sequelize');
const db = require('./db');

module.exports = db.define('fee', {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER,
  },
  fee_value: {
    allowNull: false,
    type: Sequelize.INTEGER,
  },
  payment_method: {
    allowNull: false,
    type: Sequelize.STRING,
    unique: true,
  },
}, {
  timestamps: false,
});
