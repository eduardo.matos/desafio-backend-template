const Sequelize = require('sequelize');
const db = require('./db');

module.exports = db.define('client', {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER,
  },
  name: {
    allowNull: true,
    type: Sequelize.STRING,
  },
}, {
  timestamps: false,
});
